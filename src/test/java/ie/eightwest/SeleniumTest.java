package ie.eightwest;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeleniumTest {

    @Test
    public void seleniumTest() {
//
        // specifies the location of driver executable
        System.setProperty("webdriver.chrome.driver", "c:\\chromedriver\\chromedriver_win32\\chromedriver.exe");

        // sets the chrome to run in the headless mode
        ChromeOptions chromeOptions = new ChromeOptions();
//        chromeOptions.addArguments("headless");

        ChromeDriver driver = new ChromeDriver(chromeOptions);
        driver.get("http://168.63.36.18:8080/marcin");
        WebElement e = driver.findElementByTagName("h2");

        String title = e.getText();

        System.out.println("Selenium test...");
        Assert.assertEquals("Deployed by team city",title);

        WebElement link = driver.findElementByLinkText("Home Page");
        link.click();

//        title = driver.findElementByTagName("h1").getText();
//
//        Assert.assertEquals("Home Page", title);

        driver.navigate().back();
        link = driver.findElementByLinkText("Static page");
        link.click();
        WebElement image = driver.findElementByTagName("img");

        image.click();
        driver.switchTo().alert().accept();

        driver.navigate().back();

        WebDriverWait wait = new WebDriverWait(driver, 10);

        WebElement button = wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName("button")));
        Assert.assertNotNull(button);

        button.click();
        driver.quit();
    }
}
