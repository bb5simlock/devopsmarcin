package ie.eightwest;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class DevopsIntroTest {

    @Test
    public void testMessage() {
        assertEquals(DevopsIntro.message, "Devops Test");
    }

    @Test
    public void dodgyTest(){
//        if (Math.random() < 0.33){
//            fail("fails 1/3 of the time");
//        }
//        else{
            assertEquals(DevopsIntro.message, "Devops Test");
//        }
    }

}
