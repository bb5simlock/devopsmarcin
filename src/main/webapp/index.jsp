<%@page language="java"%>
<%@page import="ie.eightwest.DevopsIntro"%>

<html>
<head>
    <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>
</head>
<body>
<h2>Deployed by team city</h2>

<a href="home">Home Page</a></br>
<a href="static/static.html">Static page</a>
<a href="users">User list</a>
<div>
    This is index.jsp
</div>
<%
    out.print(DevopsIntro.message);
%>
</body>
<script>
    setTimeout(function () {

        $('body').append("<button>Press Me</button>");
        $('button').on("click", function () {
            alert("button clicked");
        })

    }, 5000)
</script>
</html>
