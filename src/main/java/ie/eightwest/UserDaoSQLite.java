package ie.eightwest;

import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;


public class UserDaoSQLite implements UserDao {

    protected Connection conn;
    protected String filePath;
    protected final String baseUrl = "jdbc:sqlite:";
    protected String defaultPath = "c:/Users/HP201802/Desktop/SQLiteDB/users.db";

    public UserDaoSQLite(String filePath){
        this.filePath = filePath;
        openConnection(filePath);
    }


    public UserDaoSQLite() {
        openConnection(defaultPath);
    }

    public void openConnection(String url) {
        try {
            Class.forName("org.sqlite.JDBC");
            url = baseUrl + url;
            this.conn = DriverManager.getConnection(url);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public User addUser(User user) throws UserDaoException {
        try {
            String sql;
            PreparedStatement pstmt;
            if (user.getId() != -1) {
                sql = "insert into users" +
                        "( name, email, active, id) " +
                        "values(?, ?, ?, ?)";
                pstmt = conn.prepareStatement(sql);


                pstmt.setLong(4, user.getId());

            } else {
                sql = "insert into users" +
                        "(name, email, active) " +
                        "values(?, ?, ?)";
                pstmt = conn.prepareStatement(sql);


            }
            pstmt.setString(1, user.getName());
            pstmt.setString(2, user.getEmail());
            pstmt.setBoolean(3, user.isActive());

            pstmt.executeUpdate();
            pstmt.close();

            //tbd: get the id of the inserted row
            sql = "select last_insert_rowid()";
            pstmt = conn.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                user.setId(rs.getLong(1));
            }
            rs.close();
            pstmt.close();
//            Statement stmt = conn.createStatement();
//            String sql = "insert into users" +
//                    "(name, email, active) " +
//                    "values('"+user.getName()+"','"+user.getEmail()+"','"+user.isActive()+"')";
//            stmt.executeUpdate(sql);

            } catch(SQLException e){
                throw new UserDaoException("user already exists");
            }

        return user;
    }

    @Override
    public User updateUser(User user) throws UserDaoException {

        try {
            String sql = "update users set name=?, email=?, active=? where id=?";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, user.getName());
            pstmt.setString(2, user.getEmail());
            pstmt.setBoolean(3, user.isActive());
            pstmt.setLong(4, user.getId());
            pstmt.executeUpdate();
            pstmt.close();



        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;
    }

    @Override
    public void deleteUser(long id) throws UserDaoException {
        try {

            String sql = "delete  from users where id=?";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, id);
            int recordCount = pstmt.executeUpdate();
            pstmt.close();
            if (recordCount == 0){
                throw new UserDaoException("record not found: "+id);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public Collection<User> getUsers() {

        Collection<User> users = new ArrayList<>();

        try {
            Statement stmt = conn.createStatement();
            String sql = "select * from users";
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()){
                long id = rs.getLong("id");
                String name = rs.getString("name");
                String email = rs.getString("email");
                boolean active = rs.getBoolean("active");

                User user = new User(id, name, email, active);
                users.add(user);
            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return users;
    }

    @Override
    public User getUser(long id) throws UserDaoException {

        User user = null;

        try {
            String sql = "select * from users where id=?";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, id);
            ResultSet rs = pstmt.executeQuery();
            if(rs.next()){

                user = new User(id,
                        rs.getString("name"),
                        rs.getString("email"),
                        rs.getBoolean("active"));
            }
            else{
                throw new UserDaoException("user with id :"+id+" does not exist in the db");
            }
            pstmt.close();
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;
    }

    @Override
    public void close() throws UserDaoException {
        try {
            conn.close();
        } catch (SQLException e) {
            throw new UserDaoException("db closing exception");
        }
    }
}
