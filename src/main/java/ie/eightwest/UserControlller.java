package ie.eightwest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
public class UserControlller {

    @Autowired
    private UserDao udb;

    //allows to access the rqsts using the build in web server
    @CrossOrigin(origins = "*")
    @RequestMapping(method = RequestMethod.GET, value = "/users/{id}", produces = "application/json")
    public User getUser(@PathVariable long id){
        User user = null;
        try {
            user = udb.getUser(id);
            if (user==null){
                throw new UserNotFoundException();
            }
        } catch (UserDaoException e) {
            e.printStackTrace();
        }

        return user;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(method = RequestMethod.GET, value = "users", produces = "application/json")
    public Collection<User> getUsers(){
        Collection<User> users = udb.getUsers();
        return users;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(method = RequestMethod.DELETE, value = "users/{id}")
    public void deleteUser(@PathVariable long id){
        try {
            udb.deleteUser(id);
        } catch (UserDaoException e) {
            throw new UserNotFoundException();
        }
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(method = RequestMethod.POST, value = "users", produces = "application/json")
    public User addUser(@RequestBody User user){
        User addedUser = null;
        try {
            addedUser = udb.addUser(user);
        } catch (UserDaoException e) {
            throw new UserNotFoundException();
        }
        return addedUser;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(method = RequestMethod.PUT, value = "users/{id}", produces = "application/json")
    public User updateUser(@PathVariable long id, @RequestBody User user){

        //TODO: check that id matches user.getId()
        User userToBeUpdated = null;
        try {
            userToBeUpdated = udb.updateUser(user);
        } catch (UserDaoException e) {
            throw new UserNotFoundException();
        }
        return userToBeUpdated;
    }
}
