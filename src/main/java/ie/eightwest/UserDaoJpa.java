package ie.eightwest;

import org.hibernate.PersistentObjectException;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Service
public class UserDaoJpa implements UserDao {

    protected EntityManager em;

    public UserDaoJpa() {
        EntityManagerFactory emFactory = Persistence.createEntityManagerFactory("eightwest-jpa");
        this.em = emFactory.createEntityManager();
    }

    @Override
    public User addUser(User user) throws UserDaoException {
        // adding user with predefined id doesnt work
        em.getTransaction().begin();
        //automatically assigns the id returned from db
        em.persist(user);
        em.getTransaction().commit();
        return user;
    }

    @Override
    public User updateUser(User user) throws UserDaoException {
        User dbUser = em.find(User.class, user.getId());
        if (dbUser != null){
            dbUser.setName(user.getName());
            dbUser.setEmail(user.getEmail());
            dbUser.setActive(user.isActive());
        }
        else{
            throw new UserDaoException("user doesnt exist"+user.getId());
        }
        em.getTransaction().begin();
        em.persist(dbUser);

        em.getTransaction().commit();
        return user;
    }

    @Override
    public void deleteUser(long id) throws UserDaoException {
        User user = em.find(User.class, id);
        if (user == null){
            throw new UserDaoException("User not found:"+id);
        }
        em.getTransaction().begin();
        em.remove(user);
        em.getTransaction().commit();
    }

    @Override
    public Collection<User> getUsers() {
        TypedQuery<User> query = em.createQuery("select u from users u", User.class);
        List<User> list = query.getResultList();
        return list;
    }

    @Override
    public User getUser(long id) throws UserDaoException {
        User user = em.find(User.class, id);

        return user;
    }

    @Override
    public void close() throws UserDaoException {
        em.close();
    }
}
